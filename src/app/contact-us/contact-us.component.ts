import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup,Validators}from '@angular/forms'

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {


  contactsForm = new FormGroup ({

    YourName:new    FormControl('',[Validators.required , Validators.minLength(2) , Validators.maxLength(8)]),
    YourMail:new    FormControl('',[Validators.email]),
    Subject:new     FormControl('',Validators.pattern(/^[A-z]/)),
    YourMessage:new FormControl(''),

 });


 conactsList =[];

 saveForm()
 {


   this.conactsList.push(this.contactsForm.value);
   localStorage.setItem("contacts" , JSON.stringify(this.conactsList))
   console.log(this.contactsForm)

  }



  constructor() { }

  ngOnInit(): void {
  }

}
