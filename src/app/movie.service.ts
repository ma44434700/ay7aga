import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable ({providedIn:'root'})

export class MovieService {


  constructor (private _HttpClient:HttpClient) {

  }


  getTrendingAll():Observable<any>
  {
    return this._HttpClient.get("https://api.themoviedb.org/3/trending/all/week?api_key=158254c3a3043b1e70bb5362cbf95934")
  }
}
