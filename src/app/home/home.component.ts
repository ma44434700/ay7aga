import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import {MovieService} from '../movie.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
trendingAll = [];
  constructor(private _MovieService:MovieService) {

     _MovieService.getTrendingAll().subscribe((containner) => {
       this.trendingAll = containner.results;
     })

   }

  ngOnInit(): void {
  }

}
